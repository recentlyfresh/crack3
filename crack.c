#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

int qcompare( const void *a, const void *b)
{
    const char *one =*((char **)a);
    const char *two =*((char **)b);
    printf("comparing %s to %s\n", a, b);
  
    return strncmp(one,two, HASH_LEN);
  
}

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
       // Hash the guess using MD5
    char* g =md5(guess,strlen(guess));

    
    // Compare the two hashes
    int valid = 0;
    if(strcmp(hash,g)==0){
        valid=1;
       // printf("%s\n",guess);
    }
    // Free any malloc'd memory
    free(g);
    
    return valid;
}

// TODO
// Read in the hash file and return the array of strings.
char **read_hashes(char *filename)
{
     //mallock space for the entire file
    struct stat st;
    
    if (stat(filename, &st) == -1)
    {
        fprintf(stderr, "Can't get info about %s\n", filename);
        exit(1);
    }
    int len = st.st_size;
    
    char *file=malloc(len);
    
    //read entire file into memory
    FILE *f = fopen(filename,"r");
    if(!f){
        printf("cant open %s for read\n",filename);
        exit(1);
    } 
    fread(file,1,len,f);
    
    //replace \n with \o
    int count=0;
    for(int i=0;i<len;i++){
        if(file[i]=='\n'){
             file[i]='\0'; 
             count++;
        }
    }
    
    //mallock space for character pointers
    char ** line=malloc((count+1)*sizeof(char*));
    
    //fill in the address
    int word=0;
    line [word]= file; //the first word in the file'
    word++;
    
    for(int i=0;i<len;i++){
        if(file[i]=='\0'&&i+1<len){
            line[word]=&file[i+1];
            word++;
        }
        
    }
    
    line[word]=NULL;
    
    //return address of second array
    //*size=word-1;
    return line;
}


// TODO
// Read in the dictionary file and return the data structure.
// Each entry should contain both the hash and the dictionary
// word.
char **read_dict(char *filename)
{
     FILE *f = fopen(filename, "r");
     if (!f)
        {
        printf("read_dict: file error message\n");
        exit(1);
        }
    
    int arrlen = 0;
    int i =  0;
    char **dict = NULL;
    char buf[PASS_LEN];
    while (fgets(buf, PASS_LEN, f) != NULL)
        {
        if (i == arrlen)
            {
            arrlen += 1;
            char **newdict = realloc(dict, arrlen * sizeof(char*));
            if (!newdict)
                {
                printf("read_dict: newdict error message\n");
                exit(1);
                }
            dict = newdict;
            }// end of if
    
        buf[strlen(buf) - 1] = '\0';
        int slen = strlen(buf);
        char *pass = malloc( (slen + 1) * sizeof(char));
        strcpy(pass, buf);
    
        char output[(HASH_LEN + PASS_LEN + 1)];
        sprintf(output, "%s:%s", md5(pass, strlen(pass)), pass );
    
        dict[i] = strdup(output);
        i++;
        }// end of while
    
    if (i == arrlen)
        {
        char **newarr = realloc(dict, (arrlen + 1) * sizeof(char*));
        if (!newarr)
            {
            printf("read_dict: newarr error message\n");
            exit(1);
            }
        dict = newarr;
        }
    
    dict[i] = NULL;
    return dict;
}// end of read_dict


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1]);

    // TODO: Read the dictionary file into an array of strings
    char **dict = read_dict(argv[2]);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    //qsort(dict, ___, ___, ___);
    
    for (int i = 0; dict[i] != NULL; i++)
    {
    printf("%s\n", dict[i]);
    }
    
    
    qsort(dict[0], (sizeof(dict) / sizeof(dict[0])), sizeof(char*), qcompare);

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    
    FILE *f = fopen("output.txt", "w");
    if (!f)
    {
        printf("Can't open %s for reading\n", argv[1]);
    }
    /*   
    int i = 0;
    int valid=0;
    while(hashes[i]!=NULL)
    {
        
        int d=0;
        while(valid==0){
            valid=tryguess(hashes[i],dict[d]);
            if (valid==1){
                fprintf(f,"%s\n",dict[d]);
            }
            d++;
        }
        //printf("%s\n",hashes[i]);
        i++;
        valid=0;
    }
    */
    
    fclose(f);
    
    for (int i = 0; dict[i] != NULL; i++)
    {
    printf("%s\n", dict[i]);
    }
}
